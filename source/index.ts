import {
    merge,
    timer,
    fromEvent
} from 'https://unpkg.com/rxjs@6.5.3/_esm5/index.js?module';
import {
    startWith,
    map,
    switchMap,
    pluck,
    tap,
    distinct,
    catchError
} from 'https://unpkg.com/rxjs@6.5.3/_esm5/operators/index.js?module';
// import { d3 } from '../node_modules/d3/dist/d3.js';

type Observation = { id: string, timestamp: Date, temperature: number };

const API_URL = 'https://api.weather.gov';

/**
 * Helper function to transform a date into the correct format for API calls.
 * @param date the date to stringify for API calls
 */
function toAPIDateString(date: Date) {
    return date.toISOString().slice(0, -5) + 'Z';
}

/**
 * Wrapper for the API calls. Returns geoJSON.
 * @param endpoint the API endpoint
 * @param params parameters to include in the API call
 */
async function api(endpoint: string, params: URLSearchParams = new URLSearchParams()) {
  const url = `${API_URL}${endpoint}?${params}`;
  const resp = await fetch(url, { headers: { 'accept': 'application/geo+json' }});
  return resp.json();
}

/**
 * Retrieves a list of observations at a given station.
 * @param stationId the 4-letter station identifier
 * @param start the ISO date representing the start of the window
 * @param end the ISO date representing the end of the window
 * @param limit the maximum number of observations to return
 */
function getObservations(stationId: string, start?: Date, end?: Date, limit?: number): Promise<Observation[]> {
    const params = new URLSearchParams();
    if (typeof start !== 'undefined') params.append('start', toAPIDateString(start));
    if (typeof end !== 'undefined') params.append('end', toAPIDateString(end));
    if (typeof limit !== 'undefined') params.append('limit', limit.toFixed(0));
    return api(`/stations/${stationId}/observations`, params);
}

/**
 * Retrieves a list of all stations known by the API.
 * @param limit the maximum number of stations to return
 */
async function getStations(limit?: number): Promise<any> {
    const params = new URLSearchParams();
    if (typeof limit !== 'undefined') params.append('limit', limit.toFixed(0));
    return (await api('/stations', params)).features;
}

/**
 * Plots a time-series of the observations using d3.
 * @param data The list of observations to draw
 */
function draw(data: Observation[]) {
    const margin = { top: 32, right: 32, bottom: 64, left: 64 };
    const width = 960 - margin.left - margin.right;
    const height = 500 - margin.top - margin.bottom;

    // Setup svg
    const svg = d3.select('figure svg')
        .attr('viewbox', `0 0 960 500`)
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom);

    // Clear svg
    svg.selectAll('*').remove();

    // Create plot
    const plot = svg.append('g')
        .attr('transform', `translate(${margin.left},${margin.top})`);

    // Create horizontal scale
    const x = d3.scaleTime()
        .domain(d3.extent(data, (obs: Observation) => obs.timestamp))
        .range([0, width]);

    // Add horizontal axis
    plot.append('g')
        .attr('class', 'axis')
        .attr('transform', `translate(0, ${height})`)
        .call(d3.axisBottom(x))
        .append('text')
            .attr('class', 'label')
            .attr('text-anchor', 'middle')
            .attr('x', width / 2)
            .attr('y', 48)
            .text('Date & Time');

    // Create vertical scale, adding padding suited for temperatures in degrees Celsius
    const y = d3.scaleLinear()
        .domain([
            Math.min(0, d3.min(data, d => d.temperature) - 5),
            Math.max(35, d3.max(data, d => d.temperature) + 5)
        ])
        .range([height, 0]);

    // Add vertical axis
    plot.append('g')
        .attr('class', 'axis')
        .call(d3.axisLeft(y))
        .append('text')
            .attr('class', 'label')
            .attr('text-anchor', 'middle')
            .attr('x', -height / 2)
            .attr('y', -32)
            .attr('transform', 'rotate(-90)')
            .text('Temperature (°C)');

    // Define line
    const valueline = d3.line()
        .x(d => x(d.timestamp))
        .y(d => y(d.temperature))
        .defined(d => d.temperature)
        .curve(d3.curveMonotoneX);

    // Add line, binding to data
    plot.append('path')
        .datum(data)
        .attr('class', 'line')
        .attr('d', valueline);

    // Add data point markers, binding to data
    plot.selectAll('circle')
        .data(data)
        .join(
            enter => enter.append('circle')
                .attr('r', 0)
                .attr('cx', d => x(d.timestamp))
                .attr('cy', d => y(d.temperature))
                .call(enter => enter.transition().attr('r', 3)),
            update => update,
            exit => exit.call(exit => exit.transition().attr('r', 0).remove())
        );
}

/**
 * Transforms a feature into a simplified observation model.
 * @param feature a single geoJSON feature representing an observation
 */
const mapObservation = (feature: any) => ({
    id: feature.id,
    timestamp: new Date(feature.properties.timestamp),
    temperature: feature.properties.temperature.value,
});

/**
 * Transforms a list of features into a simplified model and removes null values.
 * @param features the geoJSON features representing a list of observations
 */
const transformAndRemoveNull = (features: any) => features
    .map(mapObservation)
    .filter((obs: Observation) => obs.temperature !== null);

async function init() {
    const INTERVAL = 30 * 1000;
    const MS_PER_HOUR = 1000 * 60 * 60;

    const stationSelect: HTMLSelectElement | null = document.querySelector('#stations');
    const rangeSelect: HTMLSelectElement | null = document.querySelector('#range');
    if (stationSelect === null) throw new Error('missing #stations select');
    if (rangeSelect === null) throw new Error('missing #ramge select');

    // Retrieve stations and populate <select>
    // Using a document fragment to minimize DOM operations
    const stations = await getStations();
    const frag = document.createDocumentFragment();
    for (const station of stations) {
        const option = document.createElement('option');
        const { stationIdentifier, name } = station.properties;
        option.value = stationIdentifier;
        option.textContent = `[${stationIdentifier}] ${name}`;
        frag.appendChild(option);
    }
    stationSelect.appendChild(frag);

    // Accessors for the <select> elements
    const getStation = () => stationSelect.value;
    const getStart = () => new Date(Date.now() - MS_PER_HOUR * parseInt(rangeSelect.value, 10));

    // Create the reactive data flow
    merge(
        fromEvent(stationSelect, 'change'),
        fromEvent(rangeSelect, 'change')
    ).pipe(
        startWith(null),
        switchMap(() => timer(0, INTERVAL)),
        // tap(() => console.log(getStation(), toAPIDateString(getStart()))),
        switchMap(() => getObservations(getStation(), getStart())),
        // catchError(error => console.error(error)),
        pluck('features'),
        // switchMap(features => from(features)),
        // distinct(feature => feature.id),
        map(transformAndRemoveNull),
        // tap(console.log)
    ).subscribe(draw);
}

console.log('ready');
init();
