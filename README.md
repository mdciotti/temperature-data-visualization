![Screenshot](screenshot.png)

Instructions
------------

Baseline: requires NodeJS and either NPM or Yarn (preferred).

1. Install dependencies with `npm install` or `yarn install`.
2. Run the build script with `npm run build` or `yarn build`.
3. Run the provided server with `npm run serve` or `yarn serve`.
4. Open the page at http://localhost:5000 (or wherever the serve command displays).

To-Do
-----

- [x] create d3 plot
- [x] pull data from weather.gov API
- [x] integrate RxJS
- [x] axis labels
- [ ] fix build system to use local RxJS (not unpkg)
- [ ] more d3 animations
- [ ] only update d3 data that is added/removed instead of redrawing the whole plot
- [x] documentation
- [ ] tests
